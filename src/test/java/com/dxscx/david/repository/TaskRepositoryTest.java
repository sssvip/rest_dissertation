package com.dxscx.david.repository;

import com.dxscx.david.domain.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by David on 2017/5/1.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TaskRepositoryTest {
    @Resource
    TaskRepository taskRepository;

    @Test
    public void save() throws Exception {
        Task task = new Task();
        task.setTaskExecutor("test");
        taskRepository.save(task);
    }

    @Test
    public void setTaskLaterId() throws Exception {
        int result = taskRepository.setTaskLaterIdAndTaskExecutor("tasdasdasdasdasdas", "1");
        System.out.println(result);
    }

}