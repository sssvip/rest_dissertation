package com.dxscx.david.service;

import com.dxscx.david.domain.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by David on 2017/5/1.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TaskServiceTest {
    @Resource
    TaskService taskService;

    @Test
    public void getTask() throws Exception {
        Task task = taskService.getTask("1");
        System.out.println(task);
    }

}