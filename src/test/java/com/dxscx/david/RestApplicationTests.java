package com.dxscx.david;

import com.dxscx.david.domain.Task;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApplicationTests {
    public static final String URL_PREFIX = "http://127.0.0.1/";
    //REST请求客户端
    public RestTemplate restTemplate = new RestTemplate();

    //拉取任务资源,不单独测试，在修改和删除都需要进行获取，所以算是进行集成update和delete测试。
    @Test
    public void getTask() throws Exception {

    }

    //创建任务资源
    @Test
    public void save() throws Exception {
        int thread_size = 30;
        Thread[] threads = new Thread[thread_size];
        //初始化任务线程
        for (int i = 0; i < thread_size; i++) {
            Thread save = new Thread() {
                @Override
                public void run() {
                    for (int j = 0; j < 60; j++) {
                        Random random = new Random();
                        Task task = new Task();
                        //添加所有客户端都能执行的任务
                        task.setTaskExecutor(Task.TASK_EXECUTOR_ALL_CAN);
                        //随机生成任务优先级
                        task.setTaskPriority(random.nextInt(10));
                        task.setTaskStatus(Task.TASK_STATUS_DEFAULT);
                        Task respTask = restTemplate.postForObject(URL_PREFIX + "/Task", task, Task.class);
                    }
                }
            };
            threads[i] = save;
        }
        //执行线程
        for (int i = 0; i < thread_size; i++) {
            threads[i].start();
        }
        //阻塞主线程
        for (int i = 0; i < thread_size; i++) {
            threads[i].join();
        }
        System.out.println("over...");
    }

    //修改任务资源
    @Test
    public void update() throws Exception {
        int thread_size = 10;
        Thread[] threads = new Thread[thread_size];
        //初始化任务线程
        for (int i = 0; i < thread_size; i++) {
            Thread save = new Thread() {
                @Override
                public void run() {
                    for (int j = 0; j < 10; j++) {
                        String taskExecutor = "taskExecutor" + j;
                        //获取到分发的任务
                        Task getTask = restTemplate.getForObject(URL_PREFIX + "/Task?taskExecutor=" + taskExecutor, Task.class);
                        System.out.println(LocalDateTime.now() + "执行者：" + taskExecutor + " 获取到id为" + getTask.getId() + "的任务");
                        getTask.setTaskRespNote(taskExecutor + "执行了任务");
                        getTask.setTaskStatus("已完成");
                        //获取到修改后的task
                        Task updatedTask = restTemplate.postForObject(URL_PREFIX + "/Task?taskExecutor=" + taskExecutor, getTask, Task.class);
                        System.out.println(LocalDateTime.now() + "执行者：" + taskExecutor + " 获取到id为" + updatedTask.getId() + "的任务，当前任务状态："+updatedTask.getTaskStatus());
                    }
                }
            };
            threads[i] = save;
        }
        //执行线程
        for (int i = 0; i < thread_size; i++) {
            threads[i].start();
        }
        //阻塞主线程
        for (int i = 0; i < thread_size; i++) {
            threads[i].join();
        }
        System.out.println("over...");
    }

    //删除任务资源
    @Test
    public void delete() throws Exception {
        int thread_size = 10;
        Thread[] threads = new Thread[thread_size];
        //初始化任务线程
        for (int i = 0; i < thread_size; i++) {
            Thread save = new Thread() {
                @Override
                public void run() {
                    for (int j = 0; j < 10; j++) {
                        String taskExecutor = "taskExecutor" + j;
                        //获取到分发的任务
                        Task getTask = restTemplate.getForObject(URL_PREFIX + "/Task?taskExecutor=" + taskExecutor, Task.class);
                        System.out.println(LocalDateTime.now() + "执行者：" + taskExecutor + " 获取到id为" + getTask.getId() + "的任务");
                        //删除获取到的task
                        restTemplate.delete(URL_PREFIX + "/Task/" + getTask.getId());
                        System.out.println("删除请求无异常，Id为"+getTask.getId()+"的任务删除成功");
                    }
                }
            };
            threads[i] = save;
        }
        //执行线程
        for (int i = 0; i < thread_size; i++) {
            threads[i].start();
        }
        //阻塞主线程
        for (int i = 0; i < thread_size; i++) {
            threads[i].join();
        }
        System.out.println("over...");
    }
}
