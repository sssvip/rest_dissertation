package com.dxscx.david.config;

public class Constant {
    public static final String DateTime_Format = "yyyy-MM-dd HH:mm:ss";
    public static final String Date_Format = "yyyy-MM-dd";
}
