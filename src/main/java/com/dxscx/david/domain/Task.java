package com.dxscx.david.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.dxscx.david.config.Constant;
import com.dxscx.david.converter.LocalDateTimeConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by David on 2017/5/1.
 */
@Entity(name = "tb_task")
public class Task {
    //任务Id
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;
    public static final String TASK_STATUS_DEFAULT = "未执行";
    //任务状态
    private String taskStatus;
    public static final String TASK_EXECUTOR_ALL_CAN = "ALL_CAN";
    //任务执行者
    private String taskExecutor;
    //任务优先级
    private int taskPriority;
    //任务拉取时间
    @Column(columnDefinition = "datetime")
    @Convert(converter = LocalDateTimeConverter.class)
    @JSONField(format = Constant.DateTime_Format)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = Constant.DateTime_Format)
    private LocalDateTime taskPullTime;
    //任务反馈时间
    @Column(columnDefinition = "datetime")
    @Convert(converter = LocalDateTimeConverter.class)
    @JSONField(format = Constant.DateTime_Format)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = Constant.DateTime_Format)
    private LocalDateTime taskRespTime;
    //任务反馈备注
    private String taskRespNote;
    //任务后置id
    private String taskLaterId;
    //任务创建时间
    @CreationTimestamp
    @JSONField(format = Constant.DateTime_Format)
    @JsonFormat(pattern = Constant.DateTime_Format)
    private Timestamp createdOn;
    //任务更新时间
    @UpdateTimestamp
    @JSONField(format = Constant.DateTime_Format)
    @JsonFormat(pattern = Constant.DateTime_Format)
    private Timestamp updatedOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(String taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public int getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(int taskPriority) {
        this.taskPriority = taskPriority;
    }

    public LocalDateTime getTaskPullTime() {
        return taskPullTime;
    }

    public void setTaskPullTime(LocalDateTime taskPullTime) {
        this.taskPullTime = taskPullTime;
    }

    public LocalDateTime getTaskRespTime() {
        return taskRespTime;
    }

    public void setTaskRespTime(LocalDateTime taskRespTime) {
        this.taskRespTime = taskRespTime;
    }

    public String getTaskRespNote() {
        return taskRespNote;
    }

    public void setTaskRespNote(String taskRespNote) {
        this.taskRespNote = taskRespNote;
    }

    public String getTaskLaterId() {
        return taskLaterId;
    }

    public void setTaskLaterId(String taskLaterId) {
        this.taskLaterId = taskLaterId;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", taskStatus='" + taskStatus + '\'' +
                ", taskExecutor='" + taskExecutor + '\'' +
                ", taskPriority=" + taskPriority +
                ", taskPullTime=" + taskPullTime +
                ", taskRespTime=" + taskRespTime +
                ", taskRespNote='" + taskRespNote + '\'' +
                ", taskLaterId='" + taskLaterId + '\'' +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
