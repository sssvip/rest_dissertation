package com.dxscx.david.service;

import com.dxscx.david.domain.Task;
import com.dxscx.david.repository.TaskRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * Created by David on 2017/5/1.
 */
@Service
public class TaskService {
    @Resource
    TaskRepository taskRepository;

    /*
    * 拉取一条任务，利用写锁在数据库任务表中选取1条未执行任务中写入一条唯一标识，再通过唯一标识将任务拉取出来
    * 利用写锁能防止并发问题。
    * */
    public Task getTask(String taskExecutor) {
        //生成一个随机id
        String taskLaterId = UUID.randomUUID().toString();
        int result = taskRepository.setTaskLaterIdAndTaskExecutor(taskLaterId, taskExecutor);
        if (result < 0) {
            //没有任务可以设置状态，即不能分发任务
            return null;
        }
        //查找刚设置的任务
        return taskRepository.findByTaskLaterId(taskLaterId);
    }
}
