package com.dxscx.david.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by David on 2017/5/1.
 */
@RestController
@RequestMapping("System")
public class HomeController {
    @Value("${system.name}")
    private String serverName;
    @GetMapping("/Name")
    public String name() {
        return serverName;
    }
}
