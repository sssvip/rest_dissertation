package com.dxscx.david.controller;

import com.dxscx.david.domain.Task;
import com.dxscx.david.repository.TaskRepository;
import com.dxscx.david.service.TaskService;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by David on 2017/5/1.
 */
@RestController
@RequestMapping("Task")
public class TaskController {
    @Resource
    TaskService taskService;

    @Resource
    TaskRepository taskRepository;

    //获取一条任务
    @GetMapping
    public Task getTask(String taskExecutor) {
        return taskService.getTask(taskExecutor);
    }

    @PostMapping()
    public Task save(@RequestBody Task task) {
        return taskRepository.save(task);
    }

    @PutMapping("/{id}")
    public Task update(@PathVariable String id, @RequestBody Task task) {
        //查看资源是否存在
        Task oldTask = taskRepository.findOne(id);
        //如果任务不存在，创建新任务
        if (oldTask == null) {
            return taskRepository.save(task);
        }
        //如果资源存在，修改资源
        BeanUtils.copyProperties(task, oldTask);
        //保证id的不变
        oldTask.setId(id);
        return taskRepository.save(oldTask);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        taskRepository.delete(id);
    }

}
