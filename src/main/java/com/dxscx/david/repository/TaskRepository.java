package com.dxscx.david.repository;

import com.dxscx.david.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * Created by David on 2017/5/1.
 */
public interface TaskRepository extends JpaRepository<Task, String> {
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update tb_task task set task.task_later_id =?1,task.task_executor=?2,task.task_pull_time=now(),task.task_status=\"执行中\" WHERE task.task_status=\"未执行\" and (task.task_executor=\"ALL_CAN\" or task.task_executor=?2 )ORDER BY task.task_priority desc,task.created_on asc limit 1", nativeQuery = true)
    int setTaskLaterIdAndTaskExecutor(String taskLaterId, String taskExecutor);

    Task findByTaskLaterId(String taskLaterId);
}
